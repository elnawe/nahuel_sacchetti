'use strict';

const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const config = require('./webpack.config.js');
const gulp = require('gulp');
const gutil = require('gulp-util');
const path = require('path');
const project = require('./project');
const rimraf = require('rimraf');
const runSequence = require('run-sequence');
const sass = require('gulp-sass');
const uglify = require('gulp-uglifyjs');
const webpack = require('webpack');

const assetsDestinationFolder = path.join(project.BUILD_DIR, 'assets/');
const assetsFiles = path.join(project.ASSETS_DIR, '*.*');
const htmlFiles = path.join(project.SOURCE_DIR, '**/*.html');
const jsFiles = path.join(project.SOURCE_DIR, '**/*.js');
const sassFiles = path.join(project.SOURCE_DIR, '**/*.scss');

function handleError (error) {
    console.log('ERROR: ', error);
}

gulp.task('clean-build', function (callback) {
    return rimraf(project.BUILD_DIR, callback);
});

gulp.task('html', function () {
    gulp.src(htmlFiles, {base: 'src'})
        .pipe(gulp.dest(project.BUILD_DIR));
});

gulp.task('sass', function () {
    return gulp.src(sassFiles)
        .pipe(sass().on('error', handleError))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(project.BUILD_DIR))
        .pipe(browserSync.stream());
});

gulp.task('webpack', function (callback) {
    webpack(config, function (error, stats) {
        if (error) {
            handleError(error);
        }

        gutil.log('webpack', stats.toString({
            colors: true
        }));

        callback();
        browserSync.reload();
    });
});

gulp.task('move-assets', function () {
    return gulp.src(assetsFiles)
        .pipe(gulp.dest(assetsDestinationFolder));
});

gulp.task('browser-sync', function () {
    browserSync.init({
        port: 8080,
        server: {
            baseDir: project.BUILD_DIR
        },
        ui: false
    });

    gulp.watch(jsFiles, ['webpack']);
    gulp.watch(htmlFiles, ['html', 'webpack']);
    gulp.watch(sassFiles, ['sass']);
});

gulp.task('uglify', function () {
    return gulp.src('build/bundle.js')
        .pipe(uglify())
        .pipe(gulp.dest('build/bundle.min.js'));
});

gulp.task('production', function () {
    return runSequence(
        'clean-build',
        'html',
        'move-assets',
        'sass',
        'webpack'
    );
});

gulp.task('default', function () {
    return runSequence(
        'clean-build',
        ['html', 'move-assets', 'sass', 'webpack'],
        'browser-sync'
    );
});
