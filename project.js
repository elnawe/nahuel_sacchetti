const path = require('path');

module.exports = Object.freeze({
    ASSETS_DIR: path.resolve('assets'),
    BUILD_DIR: path.resolve('build'),
    SOURCE_DIR: path.resolve('src')
});
