import about from 'components/about/about';
import aboutEdit from 'components/about-edit/about-edit';
import app from 'core/app';
import content from 'components/content/content';
import cover from 'components/cover/cover';
import navigation from 'components/navigation/navigation';
import option from 'components/option/option';
import profileInfo from 'components/profile-info/profile-info';
import settings from 'components/settings/settings';
import render from 'core/render';
import router from 'core/router';

app.register('content', content);
app.register('cover', cover);
app.register('navigation', navigation);
app.register('about', about);
app.register('about-edit', aboutEdit);
app.register('settings', settings);
app.register('option', option);
app.register('profile-info', profileInfo);

function main () {
    render.app();

    router
        .addRoute(/about/, 'about.html', render.currentPage)
        .addRoute(/about-edit/, 'about-edit.html', render.currentPage)
        .addRoute(/settings/, 'settings.html', render.currentPage)
        .addRoute(/option/, 'option.html', render.currentPage)
        .addRoute(() => router.navigate(/about/))
        .checkUrl()
        .listen();
}

function documentReady(callback) {
    if ((document.attachEvent) ? document.readyState === 'complete' : document.readyState !== 'loading'){
        main();
    } else {
        document.addEventListener('DOMContentLoaded', main);
    }
}

window.model = {
    firstName: 'Jessica',
    lastName: 'Parker',
    website: 'www.seller.com',
    phoneNumber: '94932568594',
    location: 'Newport Beach, CA'
};

window.onload = documentReady;
