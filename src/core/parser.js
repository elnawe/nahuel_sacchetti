class Parser {

    loadHTML (filename) {
        let headers = new Headers();
        headers.append('Content-Type', 'text/html');
        let configuration = {
            method: 'GET',
            headers: headers,
            cache: 'default'
        };
        let request = new Request(filename, configuration);

        return fetch(request)
            .then(response => {
                return response.text();
            })
            .then(content => {
                return content;
            });
    }
}

export default new Parser();
