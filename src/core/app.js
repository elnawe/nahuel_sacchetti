import parser from 'core/parser';
import router from 'core/router';

class App {

    constructor () {
        this.components = [];
    }

    register (name, controller) {
        let {
            link,
            model,
            template
        } = controller(router);

        this.addComponent({
            link: link,
            model: model,
            name: name,
            template: template
        });
    }

    async addComponent ({link, model, name, template}) {
        this.components.push({
            link: link,
            model: model,
            name: name,
            template: await this.getParsedTemplate(name, template)
        });
    }

    getParsedTemplate (name, template) {
        let templateFilePath = [
            'components',
            name.toLowerCase()
        ];

        if (this.isHTMLFilename(template)) {
            templateFilePath.push(template);
        } else {
            throw new Error('Component template is not an HTML file');
        }

        return parser.loadHTML(templateFilePath.join('/'));
    }

    isHTMLFilename (string) {
        let splittedString = string.split('.');

        return (splittedString[splittedString.length - 1] === 'html');
    }

    getComponentByName (name) {
        let componentIndex = this.components.map(component => {
            return component.name;
        }).indexOf(name);

        return this.components[componentIndex];
    }

    getComponentNames () {
        return this.components.map(component => {
            return component.name;
        });
    }
}

export default new App();
