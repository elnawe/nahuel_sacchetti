import render from 'core/render';

class Router {

    constructor () {
        this.routes = [];
        this.root = '/';
    }

    getUrl () {
        let match = window.location.href.match(/#(.*)$/);
        let result = (match) ? match[1] : '';

        return this.cleanPath(result);
    }

    cleanPath (path) {
        return path.toString().replace(/\/$/, '').replace(/^\//, '');
    }

    addRoute (request, template, handler) {
        if (typeof request == 'function') {
            request();
        }

        this.routes.push({
            handler: handler,
            request: request,
            template: template
        });

        return this;
    }

    removeRoute (param) {
        let index;
        let route;

        for(index = 0; index < this.routes.length; index++) {
            route = this.routes[index];

            if(route.handler === param || route.request.toString() === param.toString()) {
                this.routes.splice(index, 1);
                return this;
            }
        }

        return this;
    }

    checkUrl (path) {
        let index;
        let result = path || this.getUrl();

        for (index = 0; index < this.routes.length; index++) {
            let match = result.match(this.routes[index].request);

            if (match) {
                match.shift();

                this.routes[index].handler.apply({}, match);

                return this;
            }
        }

        return this;
    }

    listen () {
        let current = this.getUrl();
        let callback = () => {
            if (current !== this.getUrl()) {
                current = this.getUrl();
                this.checkUrl(current);
            }
        };

        clearInterval(this.interval);

        this.interval = setInterval(callback, 50);

        return this;
    }

    clearRoutes () {
        this.routes = [];
        this.root = '/';

        return this;
    }

    navigate (path) {
        path = (path) ? path : '';
        window.location.href = window.location.href.replace(/#(.*)$/, '') + '#' + path;

        return this;
    }
}

export default new Router();
