import app from 'core/app';
import router from 'core/router';

function linkScripts (link) {
    let scriptElement = document.createElement('script');

    scriptElement.type = 'text/javascript';

    for (let method in link) {
        scriptElement.innerHTML += link[method];
    };

    document.body.appendChild(scriptElement);
}

function parseVariables (template, model) {
    let index;
    let variableRegex = /\$([^\s<"]+)?/g;
    let match;

    while (match = variableRegex.exec(template)) {
        template = template.replace(match[0], window.model[match[1]]);
    }

    return template;
}

function renderApp () {
    let appNode = document.getElementById('app');
    let components = app.components;
    let index;

    components.map(component => {
        if (Object.keys(component.link).length) {
            linkScripts(component.link);
        }
    });

    for (index = 0; index < components.length; index++) {
        renderComponents(components, appNode);
    }
}

function renderComponent (component) {
    let element = document.querySelector(component.name);
    let index;

    if (!element) {
        return null;
    }

    element.outerHTML = parseVariables(component.template, component.model);

    return renderComponent(component);
}

function renderComponents (components) {
    components.map(component => {
        renderComponent(component);
    });
}

function renderCurrentPage () {
    let currentPage = router.getUrl();
    let component = app.getComponentByName(currentPage);

    let setPage = setInterval(() => {
        let pageContainer = document.querySelector('#pageContainer');

        if (pageContainer) {
            pageContainer.innerHTML = parseVariables(component.template, component.model);
            clearInterval(setPage);
        }
    }, 50);
}

export default {
    app: renderApp,
    currentPage: renderCurrentPage
};
