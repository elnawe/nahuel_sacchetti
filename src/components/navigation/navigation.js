export default (router) => {
    function navigateTo (event, path, noStyling) {
        let activeClassName = 'navigation-menu__item--active';

        if (!noStyling) {
            document.querySelectorAll('.navigation-menu__item').forEach(element => {
                element.classList.remove(activeClassName);
            });

            event.target.parentElement.classList.add(activeClassName);
        }

        window.location.href = window.location.href.replace(/#(.*)$/, '') + '#' + path;
    }

    return {
        link: {
            navigateTo: navigateTo
        },
        model: {
            firstName: 'Jessica',
            lastName: 'Parker',
            phoneNumber: '(949) 325 - 68594',
            location: 'Newport Beach CA'
        },
        template: 'navigation.html'
    };
}
