export default (router) => {
    function submitForm () {
        let newValues = {
            firstName: firstName.value,
            lastName: lastName.value,
            website: website.value,
            phoneNumber: phoneNumber.value,
            location: locationInput.value
        };

        for (let key in window.model) {
            replaceText(window.model[key], newValues[key]);
        }

        window.model = newValues;
    }

    function replaceText (oldText, newText) {
        let range;

        if (window.find) {
            while (window.find(oldText)) {
                range = window.getSelection().getRangeAt(0);

                range.deleteContents();
                range.insertNode(document.createTextNode(newText));
            }
        } else if(document.body.createTextRange) {
            range = document.body.createTextRange();

            while (range.findText(oldText)) {
                range.pasteHTML(newText);
            }
        }
    }

    return {
        link: {
            replaceText: replaceText,
            submitForm: submitForm
        },
        model: {},
        template: 'about.html'
    };
}
