'use strict';

const HTMLWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const project = require('./project');
const webpack = require('webpack');

module.exports = {
    entry: [
        path.join(project.SOURCE_DIR, 'main.js')
    ],
    module: {
        loaders: [
            {
                exclude: ['/node_modules/', '/build/'],
                include: project.SOURCE_DIR,
                loader: 'babel-loader',
                test: /\.js$/
            }
        ]
    },
    output: {
        filename: 'bundle.js',
        path: project.BUILD_DIR,
        publicPath: '/'
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true
            },
            comments: false
        })
    ],
    resolve: {
        extensions: ['.js'],
        modules: [
            project.SOURCE_DIR,
            path.resolve(__dirname, 'node_modules')
        ]
    }
};
